function validate() {
  var name = document.forms["CU"]["name1"].value;
  var email = document.forms["CU"]["email1"].value;
  var phone = document.forms["CU"]["phone1"].value;
  var reason = document.forms["CU"]["reason1"].value;
  var comment = document.forms["CU"]["info"].value;
  var inputs = document.getElementsByTagName("input");
  var errmessage="";
  console.log(name)
  console.log(email)
  console.log(phone)
  console.log(reason)
  console.log(comment)

  if (name == "") {
    errmessage+="Enter in your name. \n";
  }  
  if (email == "" && phone == "") {
    errmessage+="Enter in your email and phone number. \n";
  }  
  if (reason == "Other" && comment == "") {
    errmessage+="Please provide additional information for your reason of inquiry. \n";
  }
  if (document.CU.day1.checked != true && document.CU.day2.checked != true  && document.CU.day3.checked != true && document.CU.day4.checked != true && document.CU.day5.checked != true ) {
    errmessage+="Please select the day(s) best to contact you. \n";
     }
  if (errmessage != "") {
    alert("Please update the following: \n \n" + errmessage);
    return false;
  }
  
  for (var i = 0; i<inputs.length; i++){
        if (!inputs[i].value){
            alert("In order to provide you with the best experience, we request you fill out all information on the form.");
          return false;
         }
     }
}

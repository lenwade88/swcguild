var rolls = 0;
var money = 0;

function collectBid() {
  var bidField = parseInt(document.getElementById("bidField").value);
  money += bidField;
  document.getElementById("bidField2").innerHTML = Number(money);
  if (bidField <= 0) {
    return alert("Place a bid")
  } else if (isNaN(bidField)) {
    return alert("Enter in numerical value")
  } else {
    document.getElementById("table").style.display = "table";
    document.getElementById("play").innerHTML = "Play again?";
  }
}

function playGame() {
  var rollsarr = [];
  var moneyarr = [];
  while (money > 0) {
    rollsarr.push(rolls);
    moneyarr.push(money);
    var r1 = Math.floor(6 * Math.random()) + 1;
    var r2 = Math.floor(6 * Math.random()) + 1;
    //console.log(d1,d2);
    rolls++;
    if ((r1 + r2) == 7) {
      money += 4
    } else {
      money -= 1
    }
  }
  //console.log(moneyarr);
  //console.log(rollsarr);

  var maxTotal = Math.max(...moneyarr);
  console.log(maxTotal);
  var rollsMax = moneyarr.indexOf(maxTotal) + 1;
  console.log(rollsarr[rollsMax]);

  document.getElementById("maxTotal").innerHTML = Number(maxTotal);
  document.getElementById("rollsBroke").innerHTML = Number(rolls);
  document.getElementById("rollsMax").innerHTML = Number(rollsMax);

  rolls = 0;
};